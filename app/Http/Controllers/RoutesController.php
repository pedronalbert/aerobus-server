<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Route;

class RoutesController extends Controller
{

    public function index() {
        $routes = Route::all();

        return response()->json($routes);
    }

    public function store(Request $request) {
        $inputsData = $request->only('origen', 'destiny', 'price');

        $newRoute = new Route($inputsData);
        $newRoute->active = 1;

        if($newRoute->save()) {
            return response()->json($newRoute);
        } else {
            return response()->validation_error($newRoute->errors(), 'Ruta no ha sido registrada');
        }
    }

    public function show($id) {
        $route = Route::where('id', $id)->first();

        if($route) {
            return response()->json($route);
        } else {
            return response()->not_found('Ruta no encontrada');
        }

    }

    public function update(Request $request, $id) {
        $inputsData = $request->only('origen', 'destiny', 'price');

        $route = Route::where('id', $id)->first();

        if(!$route) {
            return response()->not_found('Ruta no encontrada');
        }

        if($route->update($inputsData)) {
            return response()->json($route);
        } else {
            return response()->validation_error($route->errors(), 'Ruta no ha sido actualizada');
        }
    }

    public function destroy($id) {
        $route = Route::where('id', $id)->first();

        if(!$route) {
            return response()->not_found('Ruta no encontrada');
        }

        $route->delete();

        return response()->json('eliminado');
    }

    public function actionActivate($id) {
        $route = Route::where('id', $id)->first();

        if(!$route) {
            return response()->not_found('Ruta no encontrada');
        }

        $route->active = 1;
        $route->save();

        return response()->json(['message' => 'Ruta ha sido activada']);
    }

    public function actionDesactivate($id) {
        $route = Route::where('id', $id)->first();

        if(!$route) {
            return response()->not_found('Ruta no encontrada');
        }

        $route->active = 0;
        $route->save();

        return response()->json(['message' => 'Ruta ha sido desactivada']);
    }
}
