<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Passenger;

class PassengersController extends Controller {

    public function index() {
        $passengers = Passenger::all();

        return response()->json($passengers);
    }

    public function store(Request $request) {
        $inputsData = $request->only('first_name', 'last_name', 'sex', 'ssn', 'ssn_tag', 'phone_number', 'phone_number_ext');

        $newPassenger = new Passenger($inputsData);

        if($newPassenger->save()) {
            return response()->json($newPassenger);
        } else {
            return response()->validation_error($newPassenger->errors(), 'Pasajero no ha sido registrado');
        }
    }

    public function show($id) {
        $passenger = Passenger::where('id', $id)->first();

        if($passenger) {
            return response()->json($passenger);
        } else {
            return response()->not_found('Pasajero no encontrado');
        }

    }


    public function update(Request $request, $id) {
        $inputsData = $request->only('first_name', 'last_name', 'sex', 'ssn', 'ssn_tag', 'phone_number', 'phone_number_ext');

        $passenger = Passenger::where('id', $id)->first();

        if(!$passenger) {
            return response()->not_found('Pasajero no encontrado');
        }

        if($passenger->update($inputsData)) {
            return response()->json($passenger);
        } else {
            return response()->validation_error($passenger->errors(), 'Pasajero no ha sido actualizado');
        }
    }

    public function destroy($id) {
        $passenger = Passenger::where('id', $id)->first();

        if(!$passenger) {
            return response()->not_found('Pasajero no encontrado');
        }

        $passenger->delete();

        return response()->json(['message' => 'Pasajero eliminado']);
    }
}
