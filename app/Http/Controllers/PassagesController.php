<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Passage;

class PassagesController extends Controller
{
    public function index() {
        $passages = Passage::all();

        return response()->json($passages);
    }

    public function store(Request $request) {
        $inputsData = $request->only('passenger_id', 'journey_id', 'seat');
        $inputsData['state'] = 'waiting';

        $newPassage = new Passage($inputsData);

        if($newPassage->save()) {
            return response()->json($newPassage);
        } else {
            return response()->validation_error($newPassage->errors());
        }
    }

    public function show($id) {
        $passage = Passage::findOrFail($id);

        return response()->json($passage);
    }

    public function update(Request $request, $id) {
        $passage = Passage::findOrFail($id);

        return response()->json($passage);
    }

    public function destroy($id) {
        $passage = Passage::findOrFail($id);

        $passage->delete();

        return response()->json($passage);
    }

    public function actionConfirm($id) {
        $passage = Passage::findOrFail($id);

        if($passage->confirm()) {
            return response()->json($passage);
        } else {
            return response()->validation_error($passage->errors());
        }
    }
}
