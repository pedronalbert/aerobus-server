<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Journey;
use App\Models\Bus;

class JourneysController extends Controller {

    public function index() {
        $journeys = Journey::all();

        return response()->json($journeys);
    }

    public function store(Request $request) {
        $inputsData = $request->only('route_id', 'bus_id', 'departure', 'arrival');
        $inputsData['state'] = 'waiting';

        $newJourney = new Journey($inputsData);

        if($newJourney->save()) {
            return response()->json($newJourney);
        } else {
            return response()->validation_error($newJourney->errors());
        }

    }

    public function show($id) {
        $journey = Journey::findOrFail($id);

        return response()->json($journey);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $inputsData = $request->only('route_id', 'bus_id', 'departure', 'arrival');

        $journey = Journey::findOrFail($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
