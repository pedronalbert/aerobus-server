<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Bus;

class BusesController extends Controller {

    public function index() {
        $buses = Bus::all();

        return response()->json($buses);
    }

    public function store(Request $request) {
        $inputsData = $request->only('number', 'brand', 'model');

        $newBus = new Bus($inputsData);
        $newBus->state = 'available';

        if($newBus->save()) {
            $newBus->createSeating();

            return response()->json($newBus);
        } else {
            return response()->validation_error($newBus->errors(), 'Bus no ha sido registrado');
        }
    }

    public function show($id) {
        $bus = Bus::where('id', $id)->first();

        if(!$bus) {
            return response()->not_found('Bus no encontrado');
        }
        
        return response()->json($bus);
    }

    public function update(Request $request, $id) {
        $inputsData = $request->only('number', 'brand', 'model', 'state');
        $recreateSeating = false;

        $bus = Bus::where('id', $id)->first();

        if(!$bus) {
            return response()->not_found('Bus no encontrado');
        }

        if(
            (strcmp($inputsData['brand'], $bus->brand) != 0) ||
            (strcmp($inputsData['model'], $bus->model) != 0)
        ){
            $recreateSeating = true;
        }

        if($bus->update($inputsData)) {
            if($recreateSeating) {
                $bus->deleteSeating();
                $bus->createSeating();
            }

            return response()->json($bus);
        } else {
            return response()->validation_error($bus->errors(), 'Bus no ha sido actualizado');
        }
    }

    public function destroy($id) {
        $bus = Bus::where('id', $id)->first();

        if(!$bus) {
            return response()->not_found('Bus no encontrado');
        }

        $bus->delete();

        return response()->json(['message' => 'Bus ha sido eliminado']);
    }
}
