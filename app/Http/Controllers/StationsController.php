<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Station;
use LaravelArdent\Ardent\Ardent;

class StationsController extends Controller {

    public function index() {
        $stations = Station::all();

        return response()->json($stations);
    }


    public function store(Request $request) {
        $inputsData = $request->only('name', 'state');

        $newStation = new Station($inputsData);

        if($newStation->save()) {
            return response()->json($newStation);
        } else {
            return response()->validation_error($newStation->errors(), 'Estacion no ha sido registrada');
        }
    }

    public function show($id) {
        $station = Station::where('id', $id)->first();

        if($station) {
            return response()->json($station);
        } else {
            return response()->not_found('Estacion no encontrada');
        }

    }

    public function update(Request $request, $id) {
        $inputsData = $request->only('name', 'state');

        $station = Station::where('id', $id)->first();

        if(!$station) {
            return response()->not_found('Estacion no encontrada');
        }

        if($station->update($inputsData)) {
            return response()->json($station);
        } else {
            return response()->validation_error($station->errors(), 'Estacion no ha sido actualizada');
        }
    }

    public function destroy($id) {
        $station = Station::where('id', $id)->first();

        if(!$station) {
            return response()->not_found('Estacion no encontrada');
        } else {
            $station->delete();

            return response()->json(['message' => 'Estacion ha sido eliminada']);
        }

    }
}
