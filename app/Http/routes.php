<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['middleware' => 'cors'], function () {
  Route::resource('stations', 'StationsController');
  Route::resource('buses', 'BusesController');
  Route::resource('passengers', 'PassengersController');
  Route::resource('journeys', 'JourneysController');

  //Passages
  Route::resource('passages', 'PassagesController');
  Route::post('passages/{id}/actions/confirm', 'PassagesController@actionConfirm');

  //Routes
  Route::resource('routes', 'RoutesController');
  Route::post('routes/{id}/actions/activate', 'RoutesController@actionActivate');
  Route::post('routes/{id}/actions/desactivate', 'RoutesController@actionDesactivate');

});