<?php
namespace App\Models;

use LaravelArdent\Ardent\Ardent;

class Station extends Ardent {
  protected $table = 'stations';

  protected $fillable = ['name', 'state'];

  public static $rules = [
    'name' => 'required',
    'state' => 'required'
  ];

  public function beforeSave() {
    $valid = true;

    if($this->validateUnique() == false) {
      $this->validationErrors->add('full_error_message', 'Estacion ya existe');
      $valid = false;
    }

    return $valid;
  }

  private function validateUnique() {
    $stationDuplicated = Station::where('name', $this->name)->where('state', $this->state);

    if($this->id != null) {
      $stationDuplicated->where('id', '!=', $this->id);
    }

    if($stationDuplicated->first()) {
      return false;
    } else {
      return true;
    }
  }
}