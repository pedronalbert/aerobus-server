<?php
namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use App\Models\Bus;
use App\Models\Passage;
use App\Models\Route;

class Journey extends Ardent {
  protected $table = "journeys";

  protected $fillable = ['route_id', 'bus_id', 'state', 'departure', 'arrival'];

  public static $rules = [
    'route_id' => 'required|exists:routes,id',
    'bus_id' => 'required|exists:buses,id',
    'state' => 'required|in:waiting,traveling,finished,canceled',
    'departure' => 'required',
    'arrival' => 'required'
  ];

  public function bus() {
    return $this->belongsTo(Bus::class);
  }

  public function passages() {
    return $this->hasMany(Passage::class);
  }

  public function route() {
    return $this->belongsTo(Route::class);
  }

  public function beforeSave() {
    $valid = true;

    if(strcmp($this->state, 'waiting') == 0) {
      $bus = $this->bus()->first();
      $route = $this->route()->first();

      if($bus->isAvailable() == false) {
        $this->validationErrors->add('bus_id', 'el bus no esta disponible');
        $valid = false;
      }

      if(!$route->active) {
        $this->validationErrors->add('route_id', 'la ruta no esta activa');
        $valid = false;
      }

      if($bus->isAvailableBetweenDates($this->departure, $this->arrival, $this->id) == false) {
        $this->validationErrors->add('departure', 'el bus no esta disponible en esta fecha');
        $this->validationErrors->add('arrival', 'el bus no esta disponible en esta fecha');
        $valid = false;
      }
    }

    return $valid;
  }

  public function seatIsAvailable($seatNumber) {
    $bus = $this->bus()->first();

    if(!$bus) return false;

    if($bus->seatIsAvailable($seatNumber) == false) return false;

    $passage = $this->passages()->where('seat', $seatNumber)->where('state', 'waiting')->first();

    if($passage) {
      return false;
    } else {
      return true;
    }
  }
}