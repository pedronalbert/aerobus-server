<?php
namespace App\Models;

use LaravelArdent\Ardent\Ardent;

class Passenger extends Ardent {
  protected $table = "passengers";

  protected $fillable = [
    'first_name',
    'last_name',
    'sex',
    'ssn',
    'ssn_tag',
    'phone_number',
    'phone_number_ext'
  ];

  public static $rules = [
    'first_name' => 'required',
    'last_name' => 'required',
    'ssn' => 'required|numeric',
    'sex' => 'required|in:m,f',
    'ssn_tag' => 'required|in:v,e',
    'phone_number' => 'required|numeric',
    'phone_number_ext' => 'required|numeric'
  ];

  public function beforeValidate() {
    if($this->id == null) {
      self::$rules['ssn'] = 'required|unique:passengers,ssn';
    } else {
      self::$rules['ssn'] = 'required|unique:passengers,ssn,'.$this->id;
    }
  }
}