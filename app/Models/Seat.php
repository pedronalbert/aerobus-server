<?php
namespace App\Models;

use LaravelArdent\Ardent\Ardent;

class Seat extends Ardent {
  protected $table = "seating";

  protected $fillable = ['bus_id', 'number', 'state'];

  public static $rules = [
    'bus_id' => 'required|exists:buses,id',
    'state' => 'required|in:available,damaged,busy',
    'number' => 'required'
  ];
}