<?php
namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use App\Models\Journey;
use App\Models\Bus;

/*
  TODO: 
    - validar que el asiento esta disponible
*/
class Passage extends Ardent {
  public $table = "passages";

  protected $fillable = ['passenger_id', 'journey_id', 'seat', 'state'];

  public static $rules = [
    'passenger_id' => 'required|exists:passengers,id',
    'journey_id' => 'required|exists:journeys,id',
    'seat' => 'required',
    'state' => 'required|in:waiting,confirmed,expired'
  ];

  public function journey() {
    return $this->belongsTo(Journey::class);
  }

  public function beforeSave() {
    if(strcmp($this->state, 'waiting') == 0) {
      $journey = $this->journey()->first();

      if(strcmp($journey->state, 'waiting') != 0) {
        $this->validationErrors->add('journey_id', 'el viaje esta cerrado');
        return false;
      }

      if($journey->seatIsAvailable($this->seat) == false) {
        $this->validationErrors->add('seat', 'asiento no esta disponible');
        return false;
      }
    }

    return true;
  }

  public function confirm() {
    //Validar que el pasaje este en espera
    if(strcmp($this->state, 'waiting') != 0) {
      $this->validationErrors->add('id', 'el pasaje no puede ser confirmado');
      return false;
    }

    $journey = $this->journey()->first();

    //validar que el viaje exista
    if(!$journey) {
      $this->validationErrors->add('journey_id', 'el viaje no existe');
      return false;
    }

    //validar que el viaje este activo
    if(strcmp($journey->state, 'waiting') != 0) {
      $this->validationErrors->add('journey_id', 'el viaje no esta activo');
      return false;
    }

    $this->state = 'confirmed';

    return $this->save();
  }

}