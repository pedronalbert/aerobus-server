<?php
namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use App\Models\Seat;
use App\Models\Journey;
use App\Validators\BusValidator;

/*
  TODO: use unique rule instead of beforeValidate
*/
class Bus extends Ardent {
  public $table = "buses";

  protected $fillable = ['number', 'brand', 'model', 'capacity', 'state'];

  public static $rules = [
    'number' => 'required|unique,buses,number',
    'brand' => 'required|bus_brand',
    'model' => 'required|bus_model',
    'state' => 'required|in:available,unavailable,damaged,traveling'
  ];

  public function seating() {
    return $this->hasMany(Seat::class);
  }

  public function journeys() {
    return $this->hasMany(Journey::class);
  }
  
  /** Model Events */

  public function beforeValidate() {
    if($this->id == null) {
      self::$rules['number'] = 'required|unique:buses,number';
    } else {
      self::$rules['number'] = 'required|unique:buses,id,'.$this->id;
    }
  }

  public function beforeSave() {
    $busValidator = new BusValidator();

    $this->capacity = $busValidator->getCapacity($this->brand, $this->model);
  }

  public function beforeDelete() {
    $this->deleteSeating();
  }

  public function createSeating() {
    $seatingAmount = $this->capacity;
    $seats = [];

    for ($i=0; $i < $seatingAmount; $i++) { 
      $seats[] = new Seat([
        "number" => $i+1,
        "state" => "available"
      ]);
    }

    $this->seating()->saveMany($seats); 
  }

  public function deleteSeating() {
    $this->seating()->delete();
  }

  public function isAvailable() {
    if(strcmp($this->state, 'available') == 0) {
      return true;
    } else {
      return false;
    }
  }

  /*
    Busca si hay journeys entre las fechas dadas
  */
  public function isAvailableBetweenDates($departure, $arrival, $exceptId) {
    $journeysBetweenDates = $this->journeys()
      ->where(function ($query) use($departure, $arrival, $exceptId) {
        $query->whereBetween('departure', [$departure, $arrival])
          ->whereNotIn('state', ['waiting', 'traveling']);

        if($exceptId != null) {
          $query->where('id', '!=', $exceptId);
        }
      })
      ->orWhere(function ($query) use($departure, $arrival, $exceptId) {
        $query->whereBetween('departure', [$departure, $arrival])
          ->whereNotIn('state', ['waiting', 'traveling']);

        if($exceptId != null) {
          $query->where('id', '!=', $exceptId);
        }
      })
      ->get();

    if($journeysBetweenDates->isEmpty()) {
      return true;
    } else {
      return false;
    }
  }

  public function seatIsAvailable($number) {
    $seating = $this->seating()->where('number', $number)->first();

    if(!$seating) return false;

    if(strcmp($seating->state, 'available') == 0) {
      return true;
    } else {
      return false;
    }
  }
}