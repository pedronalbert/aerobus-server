<?php
namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use App\Models\Station;

class Route extends Ardent {
  protected $table = "routes";

  protected $fillable = ['origen', 'destiny', 'price', 'active'];

  public static $rules = [
    'origen' => 'required|exists:stations,id|different:destiny',
    'destiny' => 'required|exists:stations,id|different:origen',
    'price' => 'required|numeric|min:0',
    'active' => 'required|boolean'
  ];

  public function origen() {
    return $this->hasOne(Station::class, 'id', 'origen');
  }

  public function destiny() {
    return $this->hasOne(Station::class, 'id', 'destiny');
  }

  public function beforeSave() {
    $valid = true;

    if($this->validateUnique() == false) {
      $this->validationErrors->add('full_error_message', 'Ya existe una ruta igual');
      $valid = false;
    }

    return $valid;
  }

  private function validateUnique() {
    $routeDuplicate = Route::where('origen', $this->origen)->where('destiny', $this->destiny);

    if($this->id != null) {
        $routeDuplicate->where('id', '!=', $this->id);
    }

    $routeDuplicate = $routeDuplicate->first();

    if($routeDuplicate) {
        return false;
    } else {
        return true;
    }
  }

}