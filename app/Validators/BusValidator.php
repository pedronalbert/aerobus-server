<?php

namespace App\Validators;

class BusValidator {
  private $brands = [
    'volvo',
    'busscar'
  ];

  private $models = [
    'volvo' => [
      'poltrona-2f' => [
        'total_capacity' => 45
      ],

      'buscama-2f' => [
        'total_capacity' => 60
      ]
    ],

    'busscar' => [
      'poltrona-2f' => [
        'total_capacity' => 45
      ],

      'buscama-2f' => [
        'total_capacity' => 60
      ]
    ]
  ];

  public function isValidBrand($brand) {
    return in_array($brand, $this->brands);
  }

  public function isValidModel($brand, $model) {
    if($this->isValidBrand($brand)) {
      return array_key_exists($model, $this->models[$brand]);
    } else {
      return false;
    }
  }

  public function getCapacity($brand, $model) {
    return $this->models[$brand][$model]['total_capacity'];
  }
}