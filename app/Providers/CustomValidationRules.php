<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Validators\BusValidator;
use Validator;

class CustomValidationRules extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {
        Validator::extend('bus_brand', function($attribute, $value, $parameters, $validator) {
            $busValidator = new BusValidator();

            return $busValidator->isValidBrand($value);
        });

        Validator::extend('bus_model', function($attribute, $value, $parameters, $validator) {
            $brand = array_get($validator->getData(), 'brand', null);
            $busValidator = new BusValidator();

            return $busValidator->isValidModel($brand, $value);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
