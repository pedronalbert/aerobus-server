<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Routing\ResponseFactory;

class CustomResponsesProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(ResponseFactory $factory) {
        $factory->macro('validation_error', function ($messageBag, $defaultFullErrorMessage = null) use ($factory) {
            $errorId = 'validation_error';
            $jsonResponse = [
                "id" => $errorId,
                "fields" => []
            ];

            $fullErrorMessage = $messageBag->get('full_error_message');

            if($fullErrorMessage) {
                $jsonResponse['message'] = $fullErrorMessage[0];
            } else {
                if($defaultFullErrorMessage != null) {
                    $jsonResponse['message'] = $defaultFullErrorMessage;
                } else {
                    $jsonResponse['message'] = 'Los datos enviados contienen errores';
                }
            }

            foreach ($messageBag->getMessages() as $key => $fieldMessages) {
                if(strcmp($key, 'full_error_message') != 0) {
                    $jsonResponse["fields"][] = [$key => $fieldMessages[0]];
                }
            }

            return $factory->make($jsonResponse, 400);
        });

        $factory->macro('not_found', function ($message) use ($factory) {
            $errorId = 'not_found';
            $jsonResponse = [
                "id" => $errorId,
                "message" => $message
            ];

            return $factory->make($jsonResponse, 404);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
