<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePassagesTable extends Migration
{
    public function up() {
        Schema::create('passages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('passenger_id');
            $table->integer('journey_id');
            $table->integer('seat');
            $table->string('state');
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('passages');
    }
}
