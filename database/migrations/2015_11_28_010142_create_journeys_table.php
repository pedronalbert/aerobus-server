<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJourneysTable extends Migration
{
    public function up() {
        Schema::create('journeys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('route_id');
            $table->integer('bus_id');
            $table->string('state');
            $table->datetime('departure');
            $table->datetime('arrival');
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('journeys');
    }
}
